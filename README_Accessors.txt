Open the solution file at: \DORM.ConcreteAccessors\DORM.ConcreteAccessors.sln
Or view the example code at: \DORM.Examples\DORM.Examples.sln

It appears that when using the Fluent NHibernate accessor you cannot generate the database schema automatically in MS SQL Server 2014 but it works on previous versions... will still look at fixing this
