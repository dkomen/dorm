﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM
{
    public class OrderedProcedureParameters: IEnumerable<ProcedureParameter>
    {
        #region Private Fields
        private static object _threadLocker = "ThreadLocker";
        private List<ProcedureParameter> _orderedProcedureParameters = new List<ProcedureParameter>();
        #endregion

        #region Public Functions
        /// <summary>
        /// Add a new parameter
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns>True if added else false if not added due to the parameter name already existing in the list of parameters already added</returns>
        public bool Add(ProcedureParameter parameter)
        {
            lock (_threadLocker)
            {
                if (_orderedProcedureParameters.Contains(parameter))
                {
                    return false;
                }
                else
                {
                    _orderedProcedureParameters.Add(parameter);
                    return true;
                }
            }
        }

        /// <summary>
        /// Remove the parameter from the list that has the same name as the supplied parameterName
        /// </summary>
        /// <param name="parameterName"></param>
        public void Delete(string parameterName)
        {
            lock (_threadLocker)
            {
                ProcedureParameter parameter = new ProcedureParameter(){ Name = parameterName};
                ProcedureParameter parameterFound = _orderedProcedureParameters.Find((x) => { return x == parameter; });
                if (parameterFound != null)
                {
                    _orderedProcedureParameters.Remove(parameterFound);
                }
            }
        }

        public ProcedureParameter Index(int index)
        {
            return _orderedProcedureParameters[index];
        }
        #endregion

        public IEnumerator<ProcedureParameter> GetEnumerator()
        {
            return _orderedProcedureParameters.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _orderedProcedureParameters.GetEnumerator();
        }
    }
}
