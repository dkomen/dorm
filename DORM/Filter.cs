﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o)
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace DORM
{   
    /// <summary>
    /// A set of filter items to use when querying the datastore
    /// </summary>
    public class Filter : Interfaces.IFilter
    {
        #region Fields
        private static object _threadLocker = "ThreadLocker";
        #endregion

        #region Constructors
        public Filter()
        { }
        #endregion

        #region Properties
        /// <summary>
        /// Filter item containing a series of filter criteria
        /// </summary>
        public FilterItem FilterItem { get; set; }

        #endregion

        #region Public Functions
        /// <summary>
        /// Add a new filter item to the list of filter items
        /// </summary>
        /// <param name="filterItem">A new filter item to add to the list of filter items</param>
        public FilterItem Create(FilterJoinType filterJoinType)
        {
            lock (_threadLocker)
            {
                FilterItem = filterJoinType.ParentFilterItem;
                return FilterItem;
            }
        }
        /// <summary>
        /// Add a single filter item to the filter
        /// </summary>
        /// <param name="key">The value pointed to by the Key which to use on the left side of the comparison</param>
        /// <param name="comparitor">The comparitor to use in the equality equation</param>
        /// <param name="filterValue">The value, on the right side of the comparison, to compare to the value pointed to by the Key</param>
        /// <returns>A FilterItem containing the new filter equation</returns>
        public FilterItem Create(string key, Enumerations.FilterCriteriaComparitor comparitor, object filterValue)
        {
            lock (_threadLocker)
            {
                FilterItem = new FilterItem(new FilterItem(null).Add(key, comparitor, filterValue));
                return FilterItem;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter">The filter item to parse through and find the root\first filterItem of </param>
        /// <returns></returns>
        public FilterItem GetRootFilterItem(Interfaces.IFilter filter)
        {
            if (filter == null || filter.FilterItem == null)
            {
                return null;
            }
            else
            {
                FilterItem currentFilterItem = filter.FilterItem;
                while (currentFilterItem.ParentJoinType != null)
                {
                    currentFilterItem = currentFilterItem.ParentJoinType.ParentFilterItem;
                }
                return currentFilterItem;
            }
        }

        #endregion
    }
}
