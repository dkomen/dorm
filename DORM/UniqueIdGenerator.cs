﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM
{
    /// <summary>
    /// Can be used if wanted to generate a new unique Id
    /// </summary>    
    public class UniqueIdGenerator
    {
        #region Fields
        private static int _counter = 0;
        private static object _threadLocker = "ThreadLocker";
        private static UniqueIdGenerator _instance;
        #endregion

        #region Constructors
        private UniqueIdGenerator() { }
        #endregion

        #region Public Methods
        public static UniqueIdGenerator getInstance()
        {
            if (_instance == null)
            {
                lock (_threadLocker)
                {
                    _instance = new UniqueIdGenerator();
                }
            }
            return _instance;
        }
        /// <summary>
        /// Genererate a fairly safe unique id
        /// [12][0112][231523][1234][23] = [year][month and day][hour, minute and second][millisecond][counter from 00 to 999]
        /// </summary>
        /// <returns>A long value as an Id</returns>
        public long GenerateLongId()
        {
            string key = string.Empty;
            lock (_threadLocker)
            {
                DateTime now = System.DateTime.UtcNow;
                string yearSection = (now.Year).ToString().Substring(2, 2);
                key = yearSection + now.Month.ToString().PadLeft(2, '0') + now.Day.ToString().PadLeft(2, '0') +
                            now.Hour.ToString().PadLeft(2, '0') + now.Minute.ToString().PadLeft(2, '0') + now.Second.ToString().PadLeft(2, '0') +
                            now.Millisecond.ToString().PadLeft(4, '0') + (_counter).ToString().PadLeft(3, '0');
                _counter++;

                if (_counter == 999)
                {
                    System.Threading.Thread.Sleep(1);
                    _counter = 0;
                }
            }

            return long.Parse(key);
        }
        #endregion
    }
}

