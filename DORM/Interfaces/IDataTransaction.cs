﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Interfaces
{
    /// <summary>
    /// An interface allwing for the creation of a data transaction
    /// </summary>
    /// <author name="Dean Komen" date="2012/10/30" mantisLog="" />
    public interface IDataTransaction: IDisposable
    {
        #region Transactions
        /// <summary>
        /// Create a new persistance transaction
        /// </summary>
        /// <author name="Dean Komen" date="2012/11/16" mantisLog="" />        
        ITransaction TransactionCreate();              
        #endregion
    }
}
