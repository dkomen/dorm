﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Interfaces
{
    /// <summary>
    /// An interface exposing standard data access\manipulation functionality for a datastore
    /// </summary>
    public interface IDataAccessor: IDisposable
    {
        #region Transactions
        /// <summary>
        /// Create a new persistence transaction
        /// </summary>
        ITransaction TransactionCreate();      
        /// <summary>
        /// Create a new persistence transaction
        /// </summary>
        DORM.Interfaces.ITransaction TransactionCreate(bool withNoLock);
        #endregion
    }
}
