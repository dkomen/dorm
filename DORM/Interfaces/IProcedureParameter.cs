﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Interfaces
{
    public interface IProcedureParameter: IComparable<IProcedureParameter>
    {
        /// <summary>
        /// The name of the parameter
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// The data value of the parameter
        /// </summary>
        object Value { get; set; }
        /// <summary>
        /// An alternate data type of the parameter
        /// </summary>
        /// <remarks>If not null then this data type will be used instead of the actual found data type of the 'Value' parameter</remarks>
        Type AlternateDataType { get; set; }
    }
}
