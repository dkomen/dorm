﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
namespace DORM.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface ITransaction: IDisposable
    {
        #region Properties
        /// <summary>
        /// The default maximum number of records that may be returned
        /// </summary>
        int DefaultMaximumRecordsToBeReturned { get; set; }
        #endregion

        #region Transactions
        /// <summary>
        /// Commit a currently running transaction
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollback a currently running transaction
        /// </summary>
        void Rollback();
        #endregion

        #region Data Committing
        /// <summary>
        /// Add a new or update an existing entity in the datastore
        /// </summary>
        /// <param name="entity">The entity to save\update</param>
        /// <returns>The persisted items id</returns>
        long AddOrUpdate(IEntity entity);
        #endregion

        #region Data Deletion
        /// <summary>
        /// Delete an entity from the datastore
        /// </summary>
        /// <param name="entity">The entity to delete</param>
        void Delete(Interfaces.IEntity entity);
        /// <summary>
        /// Delete many entities from the datastore
        /// </summary>
        /// <param name="fieldNameToFilterOn">The field to search on</param>
        /// <param name="filterCriteria">The fields data value for which to search for matches</param>
        void Delete<T>(string fieldNameToFilterOn, object filterCriteria)
            where T : class, DORM.Interfaces.IEntity;
        #endregion

        #region Data Retrieval
        /// <summary>
        /// Get all entities of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>        
        /// <returns></returns>
        IList<T> Retrieve<T>()
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get all entities of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <returns></returns>
        IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get all objects of a specific type by specifying a single quick filter item
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entities</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <returns></returns>
        IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get a set number of objects of a specific type by specifying a single quick filter item
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entities</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities that will be returned</param>
        /// <returns></returns>
        IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria, int maximumNumberOfEntities)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get a set number of objects of a specific type by specifying a single quick filter item
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entities</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities that will be returned</param>
        /// <returns></returns>
        IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter, int maximumNumberOfEntities)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get all entities of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="overridingFieldOrderingCriteria">Force ordering of the fields</param>
        /// <param name="filter">A filter to be used to filter the returned Entities</param>
        /// <returns></returns>
        IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity;      

        /// <summary>
        /// Get all objects of a specific type by specifying a single quick filter item
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entites</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria</param>
        /// <returns></returns>
        IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get all objects of a specific type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="overridingFieldOrderingCriteria">Force ordering of the fields</param>
        /// <param name="filter">A filter to be used to filter the returned entties</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities that will be returned</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria</param>
        /// <returns></returns>
        IList<T> Retrieve<T>(Interfaces.IFilter filter, int maximumNumberOfEntities, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get first object of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="filter">A filter to be used to filter the returned Entities</param>
        /// <returns></returns>
        T RetrieveFirst<T>(DORM.Interfaces.IFilter filter)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get first object of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="filter">A filter to be used to filter the returned Entities</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria</param>
        /// <returns></returns>
        T RetrieveFirst<T>(DORM.Interfaces.IFilter filter, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Get first object of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entities</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <returns></returns>
        T RetrieveFirst<T>(string fieldNameToFilterOn, object filterCriteria)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Retrieve one entity, from the datastore, with the specified id
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        T Retrieve<T>(long id)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Retrieve multiple entities from the datastore where the specified 'fieldName' contains data as given in the list 'inClauseCriteria'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <remarks>Think SQL: ... WHERE fieldName IN (inClauseCriteria)</remarks>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria</param>
        /// <returns></returns>
        IList<T> RetrieveWhereIn<T>(string fieldName, List<object> inClauseCriteria, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity;
        #endregion

        #region Datastore Procedures
        /// <summary>
        /// Execute a data retrieval type datastore procedure
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="procedureName">The name of the procedure</param>
        /// <param name="orderedParameters">A position ordered set of procedure parameters</param>
        /// <returns>The entity mapped results</returns>
        List<T> ProcedureGet<T>(string procedureName, OrderedProcedureParameters orderedParameters)
            where T : class, DORM.Interfaces.IEntity;

        /// <summary>
        /// Execute a data commit type datastore procedure
        /// </summary>
        /// <param name="procedureName">The name of the procedure</param>
        /// <param name="orderedParameters">A position ordered set of procedure parameters</param>
        /// <returns>The number of records effected by this execution (if available)</returns>
        int ProcedureSet(string procedureName, OrderedProcedureParameters orderedParameters);
        #endregion

        /// <summary>
        /// Do a Rollback and then close the current transaction
        /// </summary>
        void Close();
    }
}
