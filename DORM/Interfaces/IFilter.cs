﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Interfaces
{
    /// <summary>
    /// A single filter item which may be used to add to append filter items to create a completed and complex search filter
    /// </summary>
    public interface IFilter
    {
        /// <summary>
        /// Filter item containing a series of filter criteria
        /// </summary>
        FilterItem FilterItem { get; set; }

        /// <summary>
        /// Add a new filter item to the list of filter items
        /// </summary>
        /// <param name="filterItem">A new filter item to add to the list of filter items</param>
        FilterItem Create(FilterJoinType filterJoinType);

        /// <summary>
        /// Add a single filter item to the filter
        /// </summary>
        /// <param name="key">The value pointed to by the Key which to use on the left side of the comparison</param>
        /// <param name="comparitor">The comparitor to use in the equality equation</param>
        /// <param name="filterValue">The value, on the right side of the comparison, to compare to the value pointed to by the Key</param>
        /// <returns>A FilterItem containing the new filter equation</returns>
        FilterItem Create(string key, Enumerations.FilterCriteriaComparitor comparitor, object filterValue);

        FilterItem GetRootFilterItem(IFilter filter);
    }
}
