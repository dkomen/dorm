﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Attributes
{    
    /// <summary>
    /// Adds ordering information to an entities fields
    /// </summary>    
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldOrderAttribute : Attribute
    {
        #region Constructors
        /// <summary>
        /// Indicates that a entity field should be ordered
        /// </summary>
        /// <param name="orderingDirection"></param>        
        public FieldOrderAttribute(Enumerations.OrderingDirection orderingDirection)
        {
            OrderDirection = orderingDirection;
        }
        #endregion

        #region Properties
        public Enumerations.OrderingDirection OrderDirection
        {
            get;
            private set;
        }
        #endregion
    }
}
