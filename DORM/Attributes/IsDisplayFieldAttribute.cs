﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Attributes
{
    [System.AttributeUsage(AttributeTargets.Property)]
    public class IsDisplayFieldAttribute : System.Attribute
    {
        public static string GetPropertyValue<T>(T objectToQuery)
        {
            System.Reflection.PropertyInfo[] pi = objectToQuery.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo property in pi)
            {
                object[] attributesOfProperty = property.GetCustomAttributes(typeof(Attributes.IsDisplayFieldAttribute), true);
                if (attributesOfProperty.Length > 0)
                {
                    return property.GetValue(objectToQuery, null).ToString();
                }
            }
            return string.Empty;
        }
    }
}