﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o)
//
//============================================================================================
namespace DORM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// A single item that may be used in a search filter
    /// </summary>
    public class FilterItem
    {

        #region Constructors
        public FilterItem(FilterJoinType parentJointType)
        {
            ParentJoinType = parentJointType;
        }
        #endregion

        #region Properties

        /// <summary>
        /// The value pointed to by the Key which to use on the left side of the comparison
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// The comparitor to use in the equality equation
        /// </summary>
        public Enumerations.FilterCriteriaComparitor Comparitor { get; set; }

        /// <summary>
        /// The value, on the right side of the comparison, to compare to the value pointed to by the Key
        /// </summary>
        public object FilterValue { get; set; }

        public FilterJoinType ParentJoinType { get; set; }
        public FilterJoinType JoinType { get; set; }

        #endregion

        #region Public Functions
        public FilterJoinType Add(string key, Enumerations.FilterCriteriaComparitor comparitor, object filterValue)
        {
            Key = key;
            Comparitor = comparitor;
            FilterValue = filterValue;

            JoinType = new FilterJoinType(this, Enumerations.FilterJoinTypeRegion.Nothing);
            return JoinType;
        }
        #endregion
    }
}
