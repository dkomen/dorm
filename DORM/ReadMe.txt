HOW TO INSTALL DORM
===================
You can build the solution from source by opening the solution DORM.sln file in Visual Studio 10 or 12, or you can 
use the already prepared dll file named DORM.dll and its dependencies in the release or debug folders in: DORM\Bin\[debug or Release]

SOME MORE STUFFS
================
1.DORM defines a set of interfaces that indicate what a data access layer in your software suite should implement.
2.The concrete instances (in the 'ConcreteAccessors' namespace) are objects that implement the DORM interfaces and 
	expose real functionality to interact with a backend datastore.
3.Classes within the 'Attributes' namespace define custom attributes that may be used to gain added functionality to the domain 
	model, such as the ability to sort fields when retrieving them from the datastore with a data accessor.
4.All data entity properties are to be declared as 'virtual' and must implement interface: DORM.Interfaces.IEntity.
5.All datastore processing should take place in a DORM transaction.
     
