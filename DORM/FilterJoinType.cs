﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM
{
    public class FilterJoinType
    {
        public FilterJoinType(FilterItem parentFilterItem, Enumerations.FilterJoinTypeRegion joinType)
        {
            ParentFilterItem = parentFilterItem;
            JoinType = joinType;
        }

        public FilterItem ParentFilterItem { get; set; }
        public FilterItem FilterItem { get; set; }
        public Enumerations.FilterJoinTypeRegion JoinType { get; set; }

        public FilterItem SetJoinType(Enumerations.FilterJoinTypeRegion joinType)
        {
            FilterItem = new FilterItem(this);
            JoinType = joinType;
            return FilterItem;
        }
    }
}
