﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o)
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace DORM
{
    public class HelperMethods
    {
        /// <summary>
        /// Returns a string containing the string representation of the name of a class property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression"></param>
        /// /// <example>
        /// The input property is: new House.OldestChild.FirstName
        /// The result will be a string containing: "FirstName"
        /// </example>
        /// <returns></returns>
        public static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            return GetPropertyName(propertyExpression, false);
        }

        /// <summary>
        /// Returns a string representation containing the name of a class property AND if the 'returnTree' parameter == true then also returns all its parent names except the very root class name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <example>
        /// The input property is: new House.OldestChild.FirstName
        /// If returnTree == true then the result will be a string containing: "OldestChild.FirstName"
        /// If returnTree == false then the result will be a string containing: "FirstName"
        /// </example>
        /// <returns></returns>
        public static string GetPropertyName<T>(Expression<Func<T>> propertyExpression, bool returnTree)
        {                   
            object temp = (propertyExpression.Body as MemberExpression).Type.GetInterface((typeof(DORM.Interfaces.IEntity)).ToString(), true);

            if (!returnTree)
            {
                if (temp == null)
                {
                    return (propertyExpression.Body as MemberExpression).Member.Name;
                }
                else
                {
                    //The object is actually an Entity itself... so we assume the user meant to get its Id property
                    return (propertyExpression.Body as MemberExpression).Member.Name + "." + GetPropertyName(() => Activator.CreateInstance<DORM.Interfaces.IEntity>().Id);
                }
            }
            else
            {
                string builtList = string.Empty;

                string[] parts = (propertyExpression.Body as MemberExpression).ToString().Split('.');

                for (int partCounter = 1; partCounter < parts.Length; partCounter++)
                {
                    #region Is this the last object in the tree>
                    if (partCounter == parts.Length-1)
                    {                        
                        if(temp!=null)
                        {
                            //The object is actually an Entity itself... so we assume the user meant to get its Id property
                            parts[partCounter] = parts[partCounter] + "." + GetPropertyName(() => Activator.CreateInstance<DORM.Interfaces.IEntity>().Id);
                        }
                    }
                    #endregion
                    builtList += (partCounter > 1 ? "." : "") + parts[partCounter];
                }
                return builtList;
            }
        }
    }
}
