﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Enumerations
{
    /// <summary>
    /// Indicates the direction in which an entity field should be ordered when in a collection\list of other entities
    /// </summary>
    public enum OrderingDirection
    {
        /// <summary>
        /// Sort in ascending order
        /// </summary>
        /// <example>1,2,3,4,5</example>
        Ascending = 1,
        /// <summary>
        /// Sort in descending order
        /// </summary>
        /// <example>5,4,3,2,1</example>
        Descending = 2
    }
}
