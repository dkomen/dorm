﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================

namespace DORM.Enumerations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public enum FilterCriteriaComparitor
    {
        /// <summary>
        /// A==B
        /// </summary>
        Equals = 0,
        /// <summary>
        /// A!=B
        /// </summary>
        NotEquals = 1,
        /// <summary>
        /// A<B
        /// </summary>
        SmallerThan = 10,
        /// <summary>
        /// A>=B
        /// </summary>
        NotSmallerThan = 11,
        /// <summary>
        /// A>B
        /// </summary>
        GreaterThan = 20,
        /// <summary>
        /// A<=B
        /// </summary>
        NotGreaterThan = 21,
        /// <summary>
        /// A Like B. If string B starts with '%' then a like will be done like so "SELECT ... WHERE A LIKE '%B%'", else "SELECT ... WHERE A LIKE 'B%'"
        /// </summary>
        IsLike=30,
        /// <summary>
        /// A Not Like B. If string B starts with '%' then a like will be done like so "SELECT ... WHERE A NOT LIKE '%B%'", else "SELECT ... WHERE A LIKE 'B%'"
        /// </summary>
        IsNotLike=31,
        /// <summary>
        /// Find if A is in a list of options
        /// </summary>
        WhereIn=40,
        /// <summary>
        /// Find if A is NOT in a list of options
        /// </summary>
        WhereNotIn=41
    }
}
