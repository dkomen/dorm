﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Enumerations
{
    /// <summary>
    /// Indicates how to join filter items together
    /// </summary>
    /// <remarks>
    /// You might want to create a filter like so: FilterItem1 AND FilterItem2 AND FilterItem3 OR FilterItem4.
    /// THese filter regions are used to add the ANDs and OR to the statement.
    /// </remarks>
    public enum FilterJoinTypeRegion
    {
        /// <summary>
        /// Do nothing. This is the default join type on the last FilterItem
        /// </summary>
        Nothing = 0,
        /// <summary>
        /// Start a new region of sub join types. Think of an opening bracket '('
        /// </summary>
        RegionStart = 1,
        /// <summary>
        /// End a region of sub join types. Think of a closing bracket ')'
        /// </summary>
        RegionEnd = 2,
        /// <summary>
        /// Join the current filter item with the preceding filter item with an AND statement
        /// </summary>
        /// <example>Will generate a statement like: FIlterItem1 AND FilterItem2</example>
        And = 10,
        /// <summary>
        /// Join the current filter item with the preceding filter item with an OR statement
        /// </summary>
        /// <example>Will generate a statement like: FIlterItem1 OR FilterItem2</example>
        Or = 15
    }
}
