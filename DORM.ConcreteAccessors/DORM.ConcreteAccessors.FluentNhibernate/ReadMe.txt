﻿Conventions in use
------------------
1.Lazy loading is disabled by default.
2.Use 'this.GetType().Name' in a mapping class as the index or key name when manually needing to create indexes and unique keys

Tutorial
--------
1.ONE-TO-MANY RELATIONSHIPS
	On master mapping class use 'HasMany'
	On child mapping class use 'References'
	
	Example: A School object containing multiple Student objects (in a normal IList<Student> property)
	 On School mapping : Add statement 'HasMany<Student>(x => x.Students);'
	 On Student Mapping : Add statement 'References<School>(x => x.School);'

2.DECLARING A MULTI_COLUMN UNIQUE KEY 
	In the mapping class add '.UniqueKey(this.GetType().Name)' to each of the mapping statements to include in the key

3.DECLARING A MULTI_COLUMN INDEX
	In the mapping class add '.Index(this.GetType().Name)' to each of the mapping statements to include in the index

4.DECLARING A FIELD AS AN ENUMERATION
  Declaring a field with an enum type (eg. enumeration of eye colours named EyeColor) but you want to save the selected items
  integer value and NOT the default string value (eg Blue) would require '.CustomType<EyeColor>()' at the end of the mapping statement

5.FILTERING ON A CHILD PROPERTY
	We have a School class and many Student classes that reference it (see above: ONE-TO-MANY RELATIONSHIPS).
	We wish to retrieve all the Students which are in a School named "Pretoria Boys High".	

	Firstly, you will NEED TO KNOW THE ID of the school, lets assume it is: 94577346455

	Create the filter as such: 
	Filter filter = new Filter();
    FilterItem fItem = new FilterItem(null);
    filter.Create(fItem.Add("SchoolId", Enumerations.FilterCriteriaComparitor.Equals, "94577346455"));

	Or preferably as such:
	Filter filter = new Filter();
    FilterItem fItem = new FilterItem(null);
	string fieldNameToFilterOn = DORM.HelperMethods.GetPropertyName(()=>new Student().School, true)
    filter.Create(fItem.Add(fieldNameToFilterOn, Enumerations.FilterCriteriaComparitor.Equals, "94577346455"));


Unsupported features
--------------------
1. UInt .Net data type is not supported