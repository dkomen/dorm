﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace DORM.ConcreteAccessors.FluentNhibernate
{
    /// <summary>
    /// Data access\manipulation functionality for a datastore
    /// </summary>    
    /// <remarks>
    /// This is the root class for all data access functionality.
    /// You must create an instance of this class in order to persist or retrieve any data from a database
    /// </remarks>
    /// <example>
    ///  A typical usage example would be:
    ///  
    ///  string connectionString = "data source=localhost;database=DataStorage;Integrated Security=SSPI";
    ///  private DORM.ConcreteAccessors.FluentNH.FluentNHibernate<DORM.Example.Entities.Person> _dal;
    ///  _dal = new ConcreteAccessors.FluentNhibernate.Accessor<DORM.Example.Entities.Person>(connectionString, Enumerations.DatastoreType.MsSql);
    ///  
    ///  using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())<br/>
    ///  {
    ///    Entities.Person personRetrieved = transaction.Retrieve<Entities.Person>(34609873);<br/>
    ///  }
    /// </example>
    /// <typeparam name="ReferenceToSourceClass">May be any object that is declared in an assembly that contains the related entity mappings as well. It is needed so that we know where to find our entities as well as their related mappings to database tables</typeparam>
    public class Accessor<ReferenceToSourceClass> : Interfaces.IDataAccessor
        where ReferenceToSourceClass : class
    {
        #region Fields
        private static ISessionFactory _sessionFactory=null;
        private ISession _currentSession = null;
        private static object _threadLocker = "ThreadLocker";
        private DORM.Interfaces.ITransaction _currentTransactionItem = null;
        #endregion

        #region Constructors
        private Accessor() { }

        /// <summary>
        /// For speed reasons it is recommended to run this initializer at the start of your application once so as to create the singleton sessionFactory instance.. thereafter db calls will be quick
        /// </summary>
        /// <param name="connectionString"></param>        
        /// <param name="datastoreType">The type of database to connect to</param>
        /// <param name="updateDatabaseSchema">If true then the database DDL will be run so as to update the scheme</param>
        public Accessor(string connectionString, Enumerations.DatastoreType datastoreType, bool updateDatabaseSchema = false)
        {
            _sessionFactory = getSessionFactory(connectionString, datastoreType, updateDatabaseSchema);
        }
        #endregion

        #region Singleton Session
        /// <summary>
        /// Get a new singleton type instance of the nhibernate session object and optionally update\create the database schema
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="datastoreType">The type of back-end database to connect to</param>
        /// <param name="updateDatabaseSchema">If true then the database DDL will be run so as to update the scheme</param>
        /// <returns></returns>
        private ISessionFactory getSessionFactory(string connectionString, Enumerations.DatastoreType datastoreType, bool updateDatabaseSchema = false)
        {
            if (_sessionFactory == null)
            {
                lock (_threadLocker)
                {
                    _sessionFactory = CreateSessionFactory(updateDatabaseSchema, connectionString, datastoreType);
                }
            }
            return _sessionFactory;
        }        
        #endregion

        #region IDataAccessor Implementations
        /// <summary>
        /// Create a new persistance transaction
        /// </summary>
        public DORM.Interfaces.ITransaction TransactionCreate()
        {
            _currentSession = _sessionFactory.OpenSession();            
            return new Transaction(_currentSession);
        }
        /// <summary>
        /// Create a new persistance transaction
        /// </summary>
        public DORM.Interfaces.ITransaction TransactionCreate(bool withNoLock)
        {
            _currentSession = _sessionFactory.OpenSession();
            return new Transaction(_currentSession);
        }        
        #endregion

        #region Private Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>        
        /// <param name="datastoreType">The type of back-end database to connect to</param>
        /// <returns></returns>
        private ISessionFactory CreateSessionFactory(string connectionString, Enumerations.DatastoreType datastoreType)
        {
            return CreateSessionFactory(false, connectionString, datastoreType);
        }

        /// <summary>
        /// Warning: This method may result in datastore data loss! Regenerates entity model modified datastore structures. 
        /// </summary>
        /// <param name="generateSchema"></param>
        /// <param name="connectionString"></param>        
        /// <param name="datastoreType">The type of backend  database to connect to</param>
        /// <returns></returns>
        public ISessionFactory CreateSessionFactory(bool generateSchema, string connectionString, Enumerations.DatastoreType datastoreType)
        {
            try
            {
                IPersistenceConfigurer persistanceItem = null;
                switch (datastoreType)
                {
                    case Enumerations.DatastoreType.Firebird:
                    case Enumerations.DatastoreType.MySql:
                    case Enumerations.DatastoreType.Oracle:
                        throw new Exception("Firebird, Oracle and MySql is currently not implemented");
                    case Enumerations.DatastoreType.MsSql:
                        if (generateSchema)
                        {
                            persistanceItem = MsSqlConfiguration.MsSql2008.ConnectionString(connectionString).ShowSql();
                        }
                        else
                        {
                            persistanceItem = MsSqlConfiguration.MsSql2008.ConnectionString(connectionString);
                        }
                        break;
                    default:
                        throw new Exception("Unhandled database type");
                }

                if (generateSchema)
                {                    
                    return Fluently.Configure()
                        .Database(persistanceItem)
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ReferenceToSourceClass>()
                            .Conventions.Setup(c => { c.Add<CustomForeignKeyConvention>(); })
                            .Conventions.Add(FluentNHibernate.Conventions.Helpers.DefaultLazy.Never())//Disable lazy loading
                            )
                        .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(true, true)) //SchemaExport vs SchemaUpdate - Export will drop tables update will TRY to modify
                        .BuildSessionFactory();
                }
                else
                {
                    return Fluently.Configure()
                        .Database(persistanceItem)
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ReferenceToSourceClass>()
                            .Conventions.Setup(c => { c.Add<CustomForeignKeyConvention>(); })
                            .Conventions.Add(FluentNHibernate.Conventions.Helpers.DefaultLazy.Never())//Disable lazy loading
                            )
                        .BuildSessionFactory();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not 'CreateSessionFactory', error: " + ex.ToString());
            }
        }
        #endregion

        #region IDispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (disposing & !_disposed)
            {
                if (_currentSession != null)
                {
                    if (_currentTransactionItem != null)
                    {
                        _currentTransactionItem.Dispose();
                    }
                    _currentSession.Close();
                }
                _disposed = true;
            }
        }
        #endregion
    }
    
}
