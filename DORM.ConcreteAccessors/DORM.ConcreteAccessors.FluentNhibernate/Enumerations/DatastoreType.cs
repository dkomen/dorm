﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Enumerations
{
    /// <summary>
    /// The type of backend database to connect to
    /// </summary>
    public enum DatastoreType
    {
        /// <summary>
        /// Specifies the family of Microsoft SQL databases (includes SQL Server and SQL express)
        /// </summary>
        MsSql=1,
        /// <summary>
        /// Specifies an Oracle database
        /// </summary>
        Oracle=2,
        /// <summary>
        /// Specifies the Firebird database
        /// </summary>
        Firebird=3,
        /// <summary>
        /// Specifies the MySql database
        /// </summary>
        MySql=4
    }
}
