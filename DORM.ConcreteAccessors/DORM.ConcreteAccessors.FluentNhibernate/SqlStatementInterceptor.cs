﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.ConcreteAccessors.FluentNhibernate
{
    using NHibernate;
    using System.Diagnostics;

    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
            Debug.WriteLine(sql.ToString());
            return sql;
        }
    }
}
