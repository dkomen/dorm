﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.ConcreteAccessors.FluentNhibernate
{
    public class KeyGenerator : NHibernate.Id.IIdentifierGenerator
    {
        #region Fields
        #endregion

        #region IIdentifierGenerator Members

        public object Generate(NHibernate.Engine.ISessionImplementor session, object obj)
        {
            return UniqueIdGenerator.getInstance().GenerateLongId();
        }

        #endregion
    }
}
