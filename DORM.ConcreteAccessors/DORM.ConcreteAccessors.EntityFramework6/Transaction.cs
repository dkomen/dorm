﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
namespace DORM.ConcreteAccessors.EntityFramework6
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Reflection;
    using System.Data.Entity;
    
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Transaction: Interfaces.ITransaction
    {
        #region Fields
        private DbContext _currentSession;
        private ITransaction _currentTransaction;
        private static object _threadLocker = "ThreadLocker";
        private int _maximumNumberOfReturnedRecords = 100000;
        #endregion

        #region Constructors
        public Transaction(DbContext currentSession, bool withNoLock)
        {
            lock (_threadLocker)
            {

                _currentSession = currentSession;
                if (withNoLock)
                {
                    _currentTransaction = _currentSession.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
                }
                else
                {
                    _currentTransaction = _currentSession.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                }
            }
        }
        #endregion        

        #region ITransaction Implementations
        #region Transactions
        /// <summary>
        /// Commit a currently running transaction
        /// </summary>
        /// <remarks>A transaction must be committed before any save or update will actually be persisted to database</remarks>
        public void Commit()
        {
            try
            {
                _currentTransaction.Commit();
                _currentSession.Close();
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Rollback a currently running transaction
        /// </summary>
        /// <remarks>Rollback (cancel) any changes waiting to be persisted to the database</remarks>
        public void Rollback()
        {
            _currentTransaction.Rollback();
            _currentSession.Close();
        }
        #endregion

        #region Data Committing
        /// <summary>
        /// Add a new or update an existing entity in the datastore
        /// </summary>
        /// <param name="entity">The entity to save\update</param>
        /// <returns>The persisted items Id</returns>
        /// <remarks>
        /// Add an object to persist to the database to the current transaction. 
        /// Remember to commit the transaction for the requested changes to take effect.
        /// </remarks>
        public long AddOrUpdate(Interfaces.IEntity entity)
        {
            _currentSession.SaveOrUpdate(entity);
            return entity.Id;
        }       
        #endregion

        #region Data Deletion
        /// <summary>
        /// Delete an entity from the datastore
        /// </summary>
        /// <param name="entity">The entity to delete</param>
        /// <remarks>Delete an entity from the database.
        /// Remember to commit the transaction for the requested changes to take effect.
        /// </remarks>
        public void Delete(Interfaces.IEntity entity)
        {
            _currentSession.Delete(entity);
        }
        /// <summary>
        /// Delete many entities from the datastore as defined by the supplied filter information
        /// </summary>
        /// <param name="fieldNameToFilterOn">The field to search\Filter on</param>
        /// <param name="filterCriteria">The fields data value for which to search for matches</param>
        /// <remarks>
        /// Remember to commit the transaction for the requested changes to take effect.
        /// </remarks>
        public void Delete<T>(string fieldNameToFilterOn, object filterCriteria)
            where T : class, DORM.Interfaces.IEntity
        {
            DORM.Filter filter = new Filter();
            FilterItem fItem = new FilterItem(null);
            filter.Create(fItem.Add(fieldNameToFilterOn, Enumerations.FilterCriteriaComparitor.Equals, filterCriteria));

            IList<T> itemsToDelete = Retrieve<T>(fieldNameToFilterOn, filterCriteria);
            foreach (T item in itemsToDelete)
            {
                _currentSession.Delete(item);
            }
        }
        #endregion

        #region Data Retrieval

        public int DefaultMaximumRecordsToBeReturned 
        {
            get { return _maximumNumberOfReturnedRecords; }
            set { _maximumNumberOfReturnedRecords = value; }
        }
        /// <summary>
        /// Get all entities of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>        
        /// <returns></returns>
        public IList<T> Retrieve<T>()
            where T : class, DORM.Interfaces.IEntity
        {
            return Retrieve<T>(null);
        }

        /// <summary>
        /// Get all entities of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <returns></returns>
        public IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter)
            where T : class, DORM.Interfaces.IEntity
        {
            return Retrieve<T>(filter, null);
        }

        /// <summary>
        /// Get all objects of a specific type buy specifying a single quick filter item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entities</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria)
            where T : class, DORM.Interfaces.IEntity
        {
            Filter filter = new Filter();
            FilterItem fItem = new FilterItem(null);
            filter.Create(fItem.Add(fieldNameToFilterOn, Enumerations.FilterCriteriaComparitor.Equals, filterCriteria));
            return Retrieve<T>(filter);
        }

        /// <summary>
        /// Get a set number of objects of a specific type by specifying a single quick filter item
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entities</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities that will be returned</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria, int maximumNumberOfEntities)
            where T : class, DORM.Interfaces.IEntity
        {
            Filter filter = new Filter();
            FilterItem fItem = new FilterItem(null);
            filter.Create(fItem.Add(fieldNameToFilterOn, Enumerations.FilterCriteriaComparitor.Equals, filterCriteria));
            return Retrieve<T>(filter, maximumNumberOfEntities);
        }

        /// <summary>
        /// Get a set number of objects of a specific type by specifying a single quick filter item
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entities</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities that will be returned</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter, int maximumNumberOfEntities)
            where T : class, DORM.Interfaces.IEntity
        {            
            return Retrieve<T>(filter, maximumNumberOfEntities, null);
        }

        /// <summary>
        /// Get all entities of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="overridingFieldOrderingCriteria">Force ordering of the fields</param>
        /// <param name="filter">A filter to be used to filter the returned Entities</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity
        {
            ICriteria queryCriteria = CreateCriteriaForFilter<T>(filter, _currentSession);
            CreateCriteriaForFieldOrder<T>(_currentSession, queryCriteria, overridingFieldOrderingCriteria);
            return queryCriteria
                .SetMaxResults(DefaultMaximumRecordsToBeReturned)
                .List<T>();
        }
                
        /// <summary>
        /// Get all objects of a specific type by specifying a single quick filter item
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entites</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity
        {
            DORM.Filter filter = new Filter();
            FilterItem fItem = new FilterItem(null);
            filter.Create(fItem.Add(fieldNameToFilterOn, Enumerations.FilterCriteriaComparitor.Equals, filterCriteria));
            return Retrieve<T>(filter, overridingFieldOrderingCriteria);
        }

        /// <summary>
        /// Get first object of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="filter">A filter to be used to filter the returned Entities</param>
        /// <returns></returns>
        public T RetrieveFirst<T>(DORM.Interfaces.IFilter filter)
            where T : class, DORM.Interfaces.IEntity
        {
            IList<T> entitiesFound = Retrieve<T>(filter);

            if (entitiesFound.Count > 0)
            {
                return entitiesFound[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get first object of a specific type
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="filter">A filter to be used to filter the returned Entities</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria</param>
        /// <returns></returns>
        public T RetrieveFirst<T>(Interfaces.IFilter filter, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity
        {
            IList<T> entitiesFound = Retrieve<T>(filter, 1, overridingFieldOrderingCriteria);

            if (entitiesFound.Count > 0)
            {
                return entitiesFound[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieve one entity, from the datastore, with the specified id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T Retrieve<T>(long id)
            where T : class, DORM.Interfaces.IEntity
        {
            return _currentSession.Get<T>(id);
        }


        /// <summary>
        /// Get all objects of a specific type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="overridingFieldOrderingCriteria">Force ordering of the fields</param>
        /// <param name="filter">A filter to be used to filter the returned entties</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities that will be returned</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(Interfaces.IFilter filter, int maximumNumberOfEntities, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity
        {            
            ICriteria queryCriteria = CreateCriteriaForFilter<T>(filter, _currentSession);
            CreateCriteriaForFieldOrder<T>(_currentSession, queryCriteria, overridingFieldOrderingCriteria);
            return queryCriteria
                .SetMaxResults(maximumNumberOfEntities)
                .List<T>();
        }

        /// <summary>
        /// Retrieve multiple entities from the datastore where the specified 'fieldName' contains data as given in the list 'inClauseCriteria'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <remarks>Think SQL: ... WHERE fieldName IN (inClauseCriteria)</remarks>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria</param>
        /// <returns></returns>
        public IList<T> RetrieveWhereIn<T>(string fieldName, List<object> inClauseCriteria, List<OrderBy> overridingFieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity
        {
            ICriteria queryCriteria = _currentSession.CreateCriteria(typeof(T));
            queryCriteria.Add(Expression.In(fieldName, inClauseCriteria));
            CreateCriteriaForFieldOrder<T>(_currentSession, queryCriteria, overridingFieldOrderingCriteria);

            return queryCriteria
                .SetMaxResults(DefaultMaximumRecordsToBeReturned)
                .List<T>();
        }

        /// <summary>
        /// Get all objects of a specific type and return the Last one in the list 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">A filter to be used to filter the returned Entities</param>
        /// <returns></returns>
        public T RetrieveLast<T>(Interfaces.IFilter filter)
            where T : class, DORM.Interfaces.IEntity
        {
            IList<T> entitiesFound = Retrieve<T>(filter, 1, null);

            if (entitiesFound.Count > 0)
            {
                return entitiesFound[entitiesFound.Count-1];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get first object of a specific type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fieldNameToFilterOn">A fieldname to be used to filter the returned entities</param>
        /// <param name="filterCriteria">A value for the fieldname value to be filtered with</param>
        /// <returns></returns>
        public T RetrieveFirst<T>(string fieldNameToFilterOn, object filterCriteria)
            where T : class, DORM.Interfaces.IEntity
        {
            Filter filter = new Filter();
            FilterItem fItem = new FilterItem(null);
            filter.Create(fItem.Add(fieldNameToFilterOn, Enumerations.FilterCriteriaComparitor.Equals, filterCriteria));
            return RetrieveFirst<T>(filter, null);
        }        
        
        #endregion

        #region Datastore Procedures
        /// <summary>
        /// Execute a data retreival type datastore procedure
        /// </summary>
        /// <typeparam name="T">The type of mapped entity to retrieve</typeparam>
        /// <param name="procedureName">The name of the procedure</param>
        /// <param name="orderedParameters">A position ordered set of procedure parameters</param>
        /// <returns>The entity mapped results</returns>
        public List<T> ProcedureGet<T>(string procedureName, DORM.OrderedProcedureParameters orderedParameters)
            where T : class, DORM.Interfaces.IEntity
        {
            string sql = "EXEC [dbo].[" + procedureName + "] ";
            return _currentSession.Database.SqlQuery<T>(sql, orderedParameters).ToList();
        }

        /// <summary>
        /// Execute a data commit type datastore procedure
        /// </summary>
        /// <param name="procedureName">The name of the procedure</param>
        /// <param name="orderedParameters">A position ordered set of procedure parameters</param>
        /// <returns>The number of records effected by this execution (if available)</returns>
        public int ProcedureSet(string procedureName, DORM.OrderedProcedureParameters orderedParameters)
        {
            return GenerateProcedureQuery(_currentSession, null, procedureName, orderedParameters).ExecuteUpdate();
        }

        #endregion
        #endregion

        #region Private Functions
        /// <summary>
        /// Create the filter criteria entities for the session. Currently all criteria are AND'ed together
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter"></param>
        /// <param name="session"></param>
        /// <returns>A criteria object with the filter entities aded to it</returns>        
        private ICriteria CreateCriteriaForFilter<T>(Interfaces.IFilter filter, ISession session)
            where T : class, DORM.Interfaces.IEntity
        {
            ICriteria criteria = session.CreateCriteria<T>();
            if (filter != null)
            {
                Disjunction disjunction = Restrictions.Disjunction();
                Conjunction conjunction = Restrictions.Conjunction();
                FilterItem currentFilterItem = filter.GetRootFilterItem(filter);
                bool firstFilterItem = true;
                while (currentFilterItem!=null && currentFilterItem.JoinType != null)
                {
                    string fieldName = currentFilterItem.Key;
                    object value = currentFilterItem.FilterValue;

                    ICriterion criterion = null;
                    switch (currentFilterItem.Comparitor)
                    {
                        case Enumerations.FilterCriteriaComparitor.Equals:
                            if (value == null)
                            {
                                criterion = Expression.IsNull(fieldName);
                            }
                            else
                            {
                               criterion =Expression.Eq(fieldName, value);
                            }
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotEquals:
                            if (value == null)
                            {
                                criterion = Expression.IsNotNull(fieldName);
                            }
                            else
                            {
                                criterion = Expression.Not(Expression.Eq(fieldName, value));
                            }                            
                            break;
                        case Enumerations.FilterCriteriaComparitor.GreaterThan:
                            criterion = Expression.Gt(fieldName, value);
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotGreaterThan:
                            criterion = Expression.Not(Expression.Gt(fieldName, value));
                            break;
                        case Enumerations.FilterCriteriaComparitor.SmallerThan:
                            criterion = Expression.Lt(fieldName, value);
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotSmallerThan:
                            criterion = Expression.Not(Expression.Lt(fieldName, value));
                            break;
                        case Enumerations.FilterCriteriaComparitor.IsLike:
                            string stringValue = value.ToString();
                            if (stringValue.StartsWith("%"))
                            {
                                criterion = Expression.Like(fieldName, stringValue, MatchMode.Anywhere);
                            }
                            else
                            {
                                criterion = Expression.Like(fieldName, stringValue, MatchMode.Start);
                            }
                            break;    
                        case Enumerations.FilterCriteriaComparitor.IsNotLike:
                            stringValue = value.ToString();
                            if (stringValue.StartsWith("%"))
                            {
                                criterion = Expression.Not(Expression.Like(fieldName, stringValue, MatchMode.Anywhere));
                            }
                            else
                            {
                                criterion = Expression.Not(Expression.Like(fieldName, stringValue, MatchMode.Start));
                            }         
                            break;                                                  
                        case Enumerations.FilterCriteriaComparitor.WhereIn:
                            if (value as List<object> != null)
                            {
                                criterion = NHibernate.Criterion.Restrictions.In(fieldName, (List<object>)value);
                            }
                            else
                            {
                                throw new Exception("Could not create filter for FilterCriteriaComparitor.WhereIn. The filter value must be of type List<object>");
                            }
                            break;
                        case Enumerations.FilterCriteriaComparitor.WhereNotIn:
                            if (value as List<object> != null)
                            {
                                criterion = Expression.Not(NHibernate.Criterion.Restrictions.In(fieldName, (List<object>)value));
                            }
                            else
                            {
                                throw new Exception("Could not create filter for FilterCriteriaComparitor.WhereNotIn. The filter value must be of type List<object>");
                            }
                            break;
                    }
                    if (criterion != null)
                    {
                        //First
                        if (firstFilterItem)
                        {
                            firstFilterItem=false;
                            if (currentFilterItem.JoinType.JoinType == Enumerations.FilterJoinTypeRegion.Or)
                            {
                                disjunction.Add(criterion);
                            }
                            else if (currentFilterItem.JoinType.JoinType == Enumerations.FilterJoinTypeRegion.And)
                            {
                                conjunction.Add(criterion);
                            }
                            else if (currentFilterItem.JoinType.JoinType == Enumerations.FilterJoinTypeRegion.Nothing)
                            {
                                criteria.Add(criterion);
                            }
                        }
                        //Last
                        else if (currentFilterItem.JoinType.JoinType == Enumerations.FilterJoinTypeRegion.Nothing)
                        {
                            if (currentFilterItem.ParentJoinType.JoinType == Enumerations.FilterJoinTypeRegion.Or)
                            {
                                disjunction.Add(criterion);
                            }
                            else
                            {
                                conjunction.Add(criterion);
                            }
                        }
                        else if (currentFilterItem.ParentJoinType.JoinType == Enumerations.FilterJoinTypeRegion.Or)
                        {
                            disjunction.Add(criterion);
                        }
                        else if (currentFilterItem.ParentJoinType.JoinType == Enumerations.FilterJoinTypeRegion.And)
                        {
                            disjunction.Add(criterion);
                        }
                    }
                    currentFilterItem = currentFilterItem.JoinType.FilterItem;
                }
                if(disjunction.ToString()!="()") criteria.Add(disjunction);
                if (conjunction.ToString() != "()") criteria.Add(conjunction);
                
            }
            return criteria;
        }

        /// <summary>
        /// Create the field order criteria entities for the session
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session"></param>
        /// <param name="criteria">The criteria to which to add the ordering info</param>
        private void CreateCriteriaForFieldOrder<T>(ISession session, ICriteria criteria, List<OrderBy> fieldOrderingCriteria)
            where T : class, DORM.Interfaces.IEntity
        {
            if (fieldOrderingCriteria != null)
            {
                foreach (OrderBy orderingCriteria in fieldOrderingCriteria)
                {
                    switch (orderingCriteria.Ordering)
                    {
                        case Enumerations.OrderingDirection.Descending:
                            criteria.AddOrder(Order.Desc(orderingCriteria.FieldName));
                            break;
                        case Enumerations.OrderingDirection.Ascending:
                            criteria.AddOrder(Order.Asc(orderingCriteria.FieldName));
                            break;
                        default:
                            throw new Exception("Could not implement field ordering for field '" + orderingCriteria.FieldName + "' as '" + orderingCriteria.Ordering.ToString() + "' is unhandled");
                    }
                }
            }
            else
            {
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    object[] orderAttributeFields = property.GetCustomAttributes(typeof(Attributes.FieldOrderAttribute), true);
                    if (orderAttributeFields != null)
                    {
                        foreach (object orderInfo in orderAttributeFields)
                        {
                            DORM.Attributes.FieldOrderAttribute fieldOrder = (DORM.Attributes.FieldOrderAttribute)orderInfo;
                            switch (fieldOrder.OrderDirection)
                            {
                                case Enumerations.OrderingDirection.Descending:
                                    criteria.AddOrder(Order.Desc(property.Name));
                                    break;
                                case Enumerations.OrderingDirection.Ascending:
                                    criteria.AddOrder(Order.Asc(property.Name));
                                    break;
                                default:
                                    throw new Exception("Could not implement field ordering as '" + fieldOrder.OrderDirection.ToString() + "' is unhandled");
                            }
                        }
                    }
                }
            }
        }        
        #endregion

        #region Public Functions
        /// <summary>
        /// Do a Rollback and then close the current transaction
        /// </summary>
        public void Close()
        {
            Dispose();
        }
        
        #region IDispose
        /// <summary>
        /// Do a Rollback and then close the current transaction
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool _disposed = false;
        /// <summary>
        /// Do a Rollback and then close the current transaction
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing & !_disposed)
            {
                if (_currentTransaction != null && _currentTransaction.IsActive == true)
                {
                    _currentTransaction.Rollback();
                    _currentSession.Dispose();
                    _currentTransaction = null;
                }
                _disposed = true;
            }
        }
        #endregion
        #endregion        
    }
}
