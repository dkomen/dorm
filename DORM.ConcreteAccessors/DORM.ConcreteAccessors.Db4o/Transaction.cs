﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace DORM.ConcreteAccessors.Db4o
{
    public class Transaction: DORM.Interfaces.ITransaction
    {
        #region Fields
        private Db4objects.Db4o.IObjectContainer _container;
        #endregion

        #region Constructors
        public Transaction(Db4objects.Db4o.IObjectContainer container)
        {
            _container = container;
        }        
        #endregion

        #region Properties
        public int DefaultMaximumRecordsToBeReturned { get; set; }
        #endregion

        #region ITransaction implementation
        public void Commit()
        {
            _container.Commit();
        }

        public void Rollback()
        {
            _container.Rollback();
        }

        public long AddOrUpdate(Interfaces.IEntity entity)
        {
            if (entity.Id == 0)
            {
                entity.Id = DORM.UniqueIdGenerator.getInstance().GenerateLongId();
            }
            _container.Store(entity);

            return entity.Id;
        }

        public void Delete(Interfaces.IEntity entity)
        {
            _container.Delete(entity);
        }

        public void Delete<T>(string fieldNameToFilterOn, object filterCriteria) where T : class, DORM.Interfaces.IEntity
        {
            DORM.Filter filter = new Filter(fieldNameToFilterOn, filterCriteria);
            IList<T> results = Retrieve<T>(filter);
            foreach (T t in results)
            {
                _container.Delete(t);
            }
        }

        public IList<T> Retrieve<T>(Interfaces.IFilter filter, int? maximumNumberOfEntities) where T : class, DORM.Interfaces.IEntity
        {
            Db4objects.Db4o.Query.IQuery query = CreateCriteriaForFilter<T>(filter, _container);
            //Db4objects.Db4o.Query.IQuery query = _container.Query();
            //    query.Constrain(typeof(T));
            //    query.Descend("_id").Constrain(maximumNumberOfEntities).Equal();
                var allResults = query.Execute();     

            //int counter = 0;

            //IList<T> allResults = _container.Query<T>(delegate(T item)
            //{
            //    counter++;
            //    return counter<=maximumNumberOfEntities;
            //});
            System.Collections.ArrayList e = new System.Collections.ArrayList(allResults);
            return e.OfType<T>().ToList<T>();
        }

        public T RetrieveFirst<T>(Interfaces.IFilter filter) where T : class, DORM.Interfaces.IEntity
        {

            Db4objects.Db4o.Query.IQuery query = _container.Query();
            query.Constrain(typeof(T));
            query.Descend("_firstName").Constrain("Stephanie");

            var results = query.Execute();

            IList<T> entities2 = _container.Query<T>(delegate(T entity)
            {
                bool comparison = false;
                foreach (DORM.FilterItem currentCriteria in filter.FilterEntities)
                {
                    string fieldName = currentCriteria.Key;
                    object value = currentCriteria.FilterValue;
                    switch (currentCriteria.Comparitor)
                    {
                        case Enumerations.FilterCriteriaComparitor.Equals:
                            comparison = entity.GetType().GetProperty(fieldName).GetValue(entity, null).Equals(value);
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotEquals:
                            comparison = !entity.GetType().GetProperty(fieldName).GetValue(entity, null).Equals(value);
                            break;
                        case Enumerations.FilterCriteriaComparitor.GreaterThan:
                            var type = entity.GetType().GetProperty(fieldName).GetValue(entity, null);
                            //comparison = System.Convert.ChangeType(type, type.GetType().UnderlyingSystemType) >= System.Convert.ChangeType(value, type.GetType().UnderlyingSystemType);
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotGreaterThan:

                            break;
                        case Enumerations.FilterCriteriaComparitor.SmallerThan:

                            break;
                        case Enumerations.FilterCriteriaComparitor.NotSmallerThan:

                            break;
                        case Enumerations.FilterCriteriaComparitor.IsLike:

                            break;
                        case Enumerations.FilterCriteriaComparitor.IsNotLike:

                            break;
                    }
                    if (!comparison)
                    {
                        break;
                    }
                }
                return comparison;
            });

            if (entities2.Count > 0)
            {
                return entities2[0];
            }
            else
            {
                return null;
            }
        }

        public T RetrieveFirstss<T>(Interfaces.IFilter filter) where T : class, DORM.Interfaces.IEntity
        {
            IList<T> entities2 = _container.Query<T>(delegate(T entity)
            {
                bool comparison = false;
                foreach (DORM.FilterItem currentCriteria in filter.FilterEntities)
                {                    
                    string fieldName = currentCriteria.Key;
                    object value = currentCriteria.FilterValue;                    
                    switch (currentCriteria.Comparitor)
                    {
                        case Enumerations.FilterCriteriaComparitor.Equals:
                            comparison = entity.GetType().GetProperty(fieldName).GetValue(entity, null).Equals(value);                            
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotEquals:
                            comparison = !entity.GetType().GetProperty(fieldName).GetValue(entity, null).Equals(value);
                            break;
                        case Enumerations.FilterCriteriaComparitor.GreaterThan:
                            var type = entity.GetType().GetProperty(fieldName).GetValue(entity, null);
                            //comparison = System.Convert.ChangeType(type, type.GetType().UnderlyingSystemType) >= System.Convert.ChangeType(value, type.GetType().UnderlyingSystemType);
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotGreaterThan:
            
                            break;
                        case Enumerations.FilterCriteriaComparitor.SmallerThan:
                   
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotSmallerThan:
          
                            break;
                        case Enumerations.FilterCriteriaComparitor.IsLike:

                            break;
                        case Enumerations.FilterCriteriaComparitor.IsNotLike:

                            break;
                    }
                    if (!comparison)
                    {
                        break;
                    }
                }                
                return comparison;
            });

            if (entities2.Count > 0)
            {
                return entities2[0];
            }
            else
            {
                return null;
            }
        }

        public T Retrieve<T>(long id) where T : class, DORM.Interfaces.IEntity
        {                        
            IList<T> entities = _container.Query<T>(delegate(T entity) {
                return ((DORM.Interfaces.IEntity)entity).Id == id;
            });

            if (entities.Count == 1)
            {
                return entities[0];
            }
            else
            {
                return null;
            }
        }

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            Rollback();
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Create the filter criteria entities for the session. Currently all criteria are AND'ed together
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter"></param>
        /// <param name="session"></param>
        /// <returns>A criteria object with the filter entities aded to it</returns>        
        private Db4objects.Db4o.Query.IQuery CreateCriteriaForFilter<T>(Interfaces.IFilter filter, Db4objects.Db4o.IObjectContainer container)
            where T : class, DORM.Interfaces.IEntity
        {

            Db4objects.Db4o.Query.IQuery query = container.Query();
            query.Constrain(typeof(T));
            //query.Descend("Id").Constrain(1303201042550837000);

            //return query;
            
            if (filter != null)
            {
                foreach (DORM.FilterItem currentCriteria in filter.FilterEntities)
                {
                    string fieldName = currentCriteria.Key;
                    object value = currentCriteria.FilterValue;
                    switch (currentCriteria.Comparitor)
                    {
                        case Enumerations.FilterCriteriaComparitor.Equals:
                            query.Descend(fieldName).Constrain(value);
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotEquals:                          
                            query.Descend(fieldName).Constrain(value).Not();
                            break;
                        case Enumerations.FilterCriteriaComparitor.GreaterThan:
                            query.Descend(fieldName).Constrain(value);
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotGreaterThan:
                            query.Descend(fieldName).Constrain(value).Not().Greater();
                            break;
                        case Enumerations.FilterCriteriaComparitor.SmallerThan:
                            query.Descend(fieldName).Constrain(value).Smaller();
                            break;
                        case Enumerations.FilterCriteriaComparitor.NotSmallerThan:
                            query.Descend(fieldName).Constrain(value).Not().Not().Smaller();
                            break;
                        case Enumerations.FilterCriteriaComparitor.IsLike:
                            
                            break;
                        case Enumerations.FilterCriteriaComparitor.IsNotLike:
  
                            
                            break;
                    }
                }
            }
            return query;
        }
        #endregion        
        
    

        public IList<T> Retrieve<T>() where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public IList<T> Retrieve<T>(Interfaces.IFilter filter) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria, int maximumNumberOfEntities) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public IList<T> Retrieve<T>(Interfaces.IFilter filter, int maximumNumberOfEntities) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public IList<T> Retrieve<T>(Interfaces.IFilter filter, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public IList<T> Retrieve<T>(Interfaces.IFilter filter, int maximumNumberOfEntities, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public T RetrieveFirst<T>(Interfaces.IFilter filter, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public T RetrieveFirst<T>(string fieldNameToFilterOn, object filterCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public IList<T> RetrieveWhereIn<T>(string fieldName, List<object> inClauseCriteria, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public List<T> ProcedureGet<T>(string procedureName, OrderedProcedureParameters orderedParameters) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        public int ProcedureSet(string procedureName, OrderedProcedureParameters orderedParameters)
        {
            throw new NotImplementedException();
        }
    }
}
