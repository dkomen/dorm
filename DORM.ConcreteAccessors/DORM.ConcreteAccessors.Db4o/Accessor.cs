﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.ConcreteAccessors.Db4o
{
    public class Accessor : Interfaces.IDataAccessor
    {
        #region Fields
        Db4objects.Db4o.IObjectContainer _container;
        #endregion

        #region Constructors
        public Accessor(string databaseName)
        {
            _container = Db4objects.Db4o.Db4oEmbedded.OpenFile(databaseName);
        }
        #endregion

        #region IDataAccessor implementation
        public Interfaces.ITransaction TransactionCreate()
        {
            return new Transaction(_container);
        }

        public void Dispose()
        {
           
        }

        [Obsolete("Locking not implemented")]
        public Interfaces.ITransaction TransactionCreate(bool withNoLock)
        {
            throw new NotImplementedException();
        }
        #endregion
        
    }
}
