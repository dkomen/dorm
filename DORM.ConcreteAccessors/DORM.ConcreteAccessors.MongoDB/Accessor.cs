﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
namespace DORM.ConcreteAccessors.MongoDB
{
    public class Accessor : DORM.Interfaces.IDataAccessor
    {
        #region Fields
        MongoDatabase _database = null;
        #endregion

        #region Constructors
        public Accessor(string connectionString, string databaseName)
        {
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            _database = server.GetDatabase(databaseName);
        }
        #endregion

        #region IDataAccessor
        public DORM.Interfaces.ITransaction TransactionCreate()
        {
            return new Transaction(_database);
        }

        public DORM.Interfaces.ITransaction TransactionCreate(bool withNoLock)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }
        #endregion

    }
}
