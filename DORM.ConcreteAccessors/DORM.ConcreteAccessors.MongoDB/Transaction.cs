﻿//============================================================================================
//
// This file was originally written by Dean Komen for the DORM data access layer project
// The DORM project is protected by the Apache License , Version 2.0 and can be 
// viewed here: http://www.apache.org/licenses/LICENSE-2.0.html
//
// The DORM project and its direct source code is free to use as you wish, but credit
// Dean Komen and the DORM project :o) : http://www.dimension15.co.za/Pages/Dorm
//
//============================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
namespace DORM.ConcreteAccessors.MongoDB
{
    public class Transaction : DORM.Interfaces.ITransaction
    {
        #region Fields
        private MongoDatabase _database = null;
        private int _maximumResults = 100000;
        #endregion

        #region Constructors
        public Transaction(MongoDatabase database)
        {
            _database = database;
        }
        #endregion

        #region Properties
        public int DefaultMaximumRecordsToBeReturned { get; set; }
        #endregion

        /// <summary>
        /// Add a new or update an existing entity in the datastore
        /// </summary>
        /// <param name="entity">The entity to save\update</param>
        /// <returns>
        /// The persisted items id
        /// </returns>
        public long AddOrUpdate(DORM.Interfaces.IEntity entity)
        {
            var collection = _database.GetCollection(entity.GetType(), entity.GetType().Name);
            if (entity.Id == 0)
            {
                entity.Id = DORM.UniqueIdGenerator.getInstance().GenerateLongId();
                collection.Insert(entity);
            }
            else
            {
                collection.Save(entity);
            }

            return entity.Id;
        }

        /// <summary>
        /// Do a Rollback and then close the current transaction
        /// </summary>
        public void Close()
        {
        }

        /// <summary>
        /// Delete an entity from the datastore
        /// </summary>
        /// <param name="entity">The entity to delete</param>
        public void Delete(DORM.Interfaces.IEntity entity)
        {
            var collection = _database.GetCollection(entity.GetType(), entity.GetType().Name);
            var query = Query.EQ("_id", entity.Id);
            collection.Remove(query);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {

        }

        #region Private Functions
        /// <summary>
        /// Create the filter criteria entities for the session. Currently all criteria are AND'ed together
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter"></param>
        /// <param name="session"></param>
        /// <returns>A criteria object with the filter entities aaded to it</returns>        
        private IMongoQuery CreateQueryForFilter<T>(DORM.Filter filter)
            where T : class
        {
            QueryBuilder<T> queryBuilder = new QueryBuilder<T>();
            List<IMongoQuery> queries = new List<IMongoQuery>();

            if (filter != null)
            {

                FilterItem currentFilterItem = filter.GetRootFilterItem(filter);
                bool firstFilterItem = true;
                while (currentFilterItem != null && currentFilterItem.JoinType != null)
                {
                    string fieldName = currentFilterItem.Key;
                    object value = currentFilterItem.FilterValue;

                    IMongoQuery query =null;
                    switch (currentFilterItem.Comparitor)
                    {
                        case DORM.Enumerations.FilterCriteriaComparitor.Equals:
                            query = Query.EQ(fieldName, BsonValue.Create(value));
                            break;
                        case DORM.Enumerations.FilterCriteriaComparitor.NotEquals:
                            query = Query.NE(fieldName, BsonValue.Create(value));
                            break;
                        case DORM.Enumerations.FilterCriteriaComparitor.GreaterThan:
                            query = Query.GT(fieldName, BsonValue.Create(value));
                            break;
                        case DORM.Enumerations.FilterCriteriaComparitor.NotGreaterThan:
                            query = Query.LTE(fieldName, BsonValue.Create(value));
                            break;
                        case DORM.Enumerations.FilterCriteriaComparitor.SmallerThan:
                            query = Query.LT(fieldName, BsonValue.Create(value));
                            break;
                        case DORM.Enumerations.FilterCriteriaComparitor.NotSmallerThan:
                            query = Query.GTE(fieldName, BsonValue.Create(value));
                            break;
                        case DORM.Enumerations.FilterCriteriaComparitor.IsLike:
                            string stringValue = value.ToString();

                            if (stringValue.StartsWith("%"))
                            {
                                stringValue = stringValue.Remove(0, 1);
                                query = Query.Matches(fieldName, BsonRegularExpression.Create(@"/^" + BsonValue.Create(stringValue) + @"/"));
                            }
                            else
                            {
                                query = Query.Matches(fieldName, BsonRegularExpression.Create(@"/" + BsonValue.Create(stringValue) + @"/"));
                            }
                            break;
                        case DORM.Enumerations.FilterCriteriaComparitor.IsNotLike:

                            //DEANKTODO
                            break;
                    }

                    if (query != null)
                    {
                        //First
                        if (firstFilterItem)
                        {
                            firstFilterItem = false;
                            if (currentFilterItem.JoinType.JoinType == Enumerations.FilterJoinTypeRegion.Or)
                            {
                                queries.Add(Query.Or(query));
                            }
                            else if (currentFilterItem.JoinType.JoinType == Enumerations.FilterJoinTypeRegion.And)
                            {
                                queries.Add(Query.And(query));
                            }
                            else if (currentFilterItem.JoinType.JoinType == Enumerations.FilterJoinTypeRegion.Nothing)
                            {
                                queries.Add(query);
                            }
                        }
                        //Last
                        else if (currentFilterItem.JoinType.JoinType == Enumerations.FilterJoinTypeRegion.Nothing)
                        {
                            if (currentFilterItem.ParentJoinType.JoinType == Enumerations.FilterJoinTypeRegion.Or)
                            {
                                queries.Add(Query.Or(query));
                            }
                            else
                            {
                                queries.Add(Query.And(query));
                            }
                        }
                        else if (currentFilterItem.ParentJoinType.JoinType == Enumerations.FilterJoinTypeRegion.Or)
                        {
                            queries.Add(Query.Or(query));
                        }
                        else if (currentFilterItem.ParentJoinType.JoinType == Enumerations.FilterJoinTypeRegion.And)
                        {
                            queries.Add(Query.And(query));
                        }
                    }

                    currentFilterItem = currentFilterItem.JoinType.FilterItem;
                }
                
                return Query.And(queries);
            }
            else
            {
                return null;
            }
        }
        #endregion

        /// <summary>
        /// Commit a currently running transaction
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Commit()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Rollback a currently running transaction
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Rollback()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the specified field name to filter on.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fieldNameToFilterOn">The field name to filter on.</param>
        /// <param name="filterCriteria">The filter criteria.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Delete<T>(string fieldNameToFilterOn, object filterCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<T> Retrieve<T>() where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves the specified filter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter) where T : class, Interfaces.IEntity
        {
            var collection = _database.GetCollection(typeof(T), typeof(T).Name);

            //Filter                                   
            var query = CreateQueryForFilter<T>((DORM.Filter)filter);

            MongoCursor<T> resultsCursor = collection.FindAs<T>(query);

            //Max length
            resultsCursor.SetLimit(_maximumResults);

            return resultsCursor.ToList<T>();
        }

        /// <summary>
        /// Retrieves the specified field name to filter on.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fieldNameToFilterOn">The field name to filter on.</param>
        /// <param name="filterCriteria">The filter criteria.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves the specified field name to filter on.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fieldNameToFilterOn">The field name to filter on.</param>
        /// <param name="filterCriteria">The filter criteria.</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria, int maximumNumberOfEntities) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves the specified filter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<T> Retrieve<T>(Interfaces.IFilter filter, int maximumNumberOfEntities) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves the specified filter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<T> Retrieve<T>(Interfaces.IFilter filter, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves the specified field name to filter on.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fieldNameToFilterOn">The field name to filter on.</param>
        /// <param name="filterCriteria">The filter criteria.</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<T> Retrieve<T>(string fieldNameToFilterOn, object filterCriteria, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves the specified filter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities.</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<T> Retrieve<T>(Interfaces.IFilter filter, int maximumNumberOfEntities, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get first object of a specific type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public T RetrieveFirst<T>(Interfaces.IFilter filter) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get first object of a specific type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria.</param>
        /// <returns></returns>
        public T RetrieveFirst<T>(DORM.Interfaces.IFilter filter, List<DORM.OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            IList<T> entities = Retrieve<T>(filter, 1, overridingFieldOrderingCriteria);

            return entities[0];
        }

        /// <summary>
        /// Retrieves the first.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fieldNameToFilterOn">The field name to filter on.</param>
        /// <param name="filterCriteria">The filter criteria.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public T RetrieveFirst<T>(string fieldNameToFilterOn, object filterCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves the specified identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public T Retrieve<T>(long id) where T : class, Interfaces.IEntity
        {
            var collection = _database.GetCollection(typeof(T), typeof(T).Name);
            var query = Query.EQ("_id", id);
            return (T)collection.FindOneAs(typeof(T), query);
        }

        /// <summary>
        /// Retrieves the where in.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="inClauseCriteria">The in clause criteria.</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<T> RetrieveWhereIn<T>(string fieldName, List<object> inClauseCriteria, List<OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Procedures the get.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="orderedParameters">The ordered parameters.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<T> ProcedureGet<T>(string procedureName, OrderedProcedureParameters orderedParameters) where T : class, Interfaces.IEntity
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Execute a data commit type datastore procedure
        /// </summary>
        /// <param name="procedureName">The name of the procedure</param>
        /// <param name="orderedParameters">A position ordered set of procedure parameters</param>
        /// <returns>
        /// The number of records effected by this execution (if available)
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public int ProcedureSet(string procedureName, OrderedProcedureParameters orderedParameters)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Retrieves the specified filter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities.</param>
        /// <param name="overridingFieldOrderingCriteria">The overriding field ordering criteria.</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter, int? maximumNumberOfEntities, List<DORM.OrderBy> overridingFieldOrderingCriteria) where T : class, Interfaces.IEntity
        {
            var collection = _database.GetCollection(typeof(T), typeof(T).Name);

            //Filter                                   
            var query = CreateQueryForFilter<T>((DORM.Filter)filter);

            MongoCursor resultsCursor = collection.FindAs(typeof(T), query);


            //Set ordering
            foreach (DORM.OrderBy order in overridingFieldOrderingCriteria)
            {
                if (order.Ordering == DORM.Enumerations.OrderingDirection.Ascending)
                {
                    resultsCursor.SetSortOrder(SortBy.Ascending(order.FieldName));
                }
                else if (order.Ordering == DORM.Enumerations.OrderingDirection.Descending)
                {
                    resultsCursor.SetSortOrder(SortBy.Descending(order.FieldName));
                }
            }

            //Max length
            if (maximumNumberOfEntities.HasValue)
            {
                resultsCursor.SetLimit(maximumNumberOfEntities.Value);
            }
            else
            {
                resultsCursor.SetLimit(_maximumResults);
            }

            return (IList<T>)resultsCursor;
        }



        /// <summary>
        /// Retrieves the specified filter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <param name="maximumNumberOfEntities">The maximum number of entities.</param>
        /// <returns></returns>
        public IList<T> Retrieve<T>(DORM.Interfaces.IFilter filter, int? maximumNumberOfEntities) where T : class, Interfaces.IEntity
        {
            var collection = _database.GetCollection(typeof(T), typeof(T).Name);
            //Filter                                   
            var query = CreateQueryForFilter<T>((DORM.Filter)filter);

            MongoCursor<T> resultsCursor = collection.FindAs<T>(query);

            //Max length
            if (maximumNumberOfEntities.HasValue)
            {
                resultsCursor.SetLimit(maximumNumberOfEntities.Value);
            }
            else
            {
                resultsCursor.SetLimit(_maximumResults);
            }

            return (IList<T>)resultsCursor.ToList<T>();
        }        
        

    }
}
