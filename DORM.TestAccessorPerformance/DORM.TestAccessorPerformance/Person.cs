﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DORM.TestAccessorPerformance
{
    class Person : DORM.Interfaces.IEntity
    {
        private long _id;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _surname;

        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }
        
    }

    class PersonMapping : DORM.ConcreteAccessors.FluentNhibernate.EntityMap<Person>
    {
        public PersonMapping()
        {
            Map(x => x.Name);
            Map(x => x.Surname);
        }
    }
}
