﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DORM.TestAccessorPerformance
{
    class Program
    {
        static void Main(string[] args)
        {
            long iterations = 100000;
            long timeSql;
            long timeMongo;
            long timeDb4o;
            int xx = 0; ;
            try
            {
                System.Threading.Thread.Sleep(1000);
                DORM.ConcreteAccessors.Db4o.Accessor db40 = new DORM.ConcreteAccessors.Db4o.Accessor("TestDB");
                //timeDb4o = DoInsert(iterations, db40.TransactionCreate(), true);
                timeDb4o = DoRead(iterations, db40.TransactionCreate());

                DORM.ConcreteAccessors.MongoDB.Accessor mongo = new DORM.ConcreteAccessors.MongoDB.Accessor("mongodb://localhost", "PerformenceTest");
                //timeMongo = DoInsert(iterations, mongo.TransactionCreate(), false);
                timeMongo = DoRead(iterations, mongo.TransactionCreate());

                string conn = "data source=localhost;Initial Catalog=ReportServer;Integrated Security=SSPI";
                DORM.ConcreteAccessors.FluentNhibernate.Accessor<Person> nh = new DORM.ConcreteAccessors.FluentNhibernate.Accessor<Person>(conn, Enumerations.DatastoreType.MsSql);
                //timeSql = DoInsert(iterations, nh.TransactionCreate(), true);
                timeSql = DoRead(iterations, nh.TransactionCreate());

                timeDb4o = iterations / (timeDb4o / 1000);
                timeMongo = iterations / (timeMongo / 1000);
                timeSql = iterations / (timeSql / 1000);

                
            }
            catch (Exception ex)
            {
                ex = ex;
            }
        }

        private static long DoInsert(long iterations, DORM.Interfaces.ITransaction transaction, bool commitTransaction)
        {
            using (transaction)
            {
                System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
                st.Start();
                for (int i = 0; i <= iterations; i++)
                {
                    Person p = new Person() { Name = i.ToString(), Surname = @"rgf;slerjkto;aiyrtoaweiugfileh/pWJpJKFPOWEHFopjfPOwJF'POiwjfwkfPKFpo/wejfoi/wjgf/iojg/eqjog/s\jg/izer/\jgz;jgzsjg/lzjrglzojsg/;jkl\g/zj2gs/lz;jg/lzjg/o\jz/gozj\g/okr\\WRKeotjgaorg|G'" };
                    transaction.AddOrUpdate(p);
                }
                if (commitTransaction)
                {
                    transaction.Commit();
                }
                st.Stop();
                return st.ElapsedMilliseconds;
            }
        }

        private static long DoRead(long iterations, DORM.Interfaces.ITransaction transaction)
        {
            using (transaction)
            {
                System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
                st.Start();
                IList<Person> p = transaction.Retrieve<Person>(null, (int)iterations);                
                st.Stop();
                return st.ElapsedMilliseconds;
            }
        }

    }
}
