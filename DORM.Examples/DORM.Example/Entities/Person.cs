﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Example.Entities
{
    /// <summary>
    /// A entity that contains a person's details
    /// </summary>
    /// <remarks>
    /// Any property that will actually map to the database must be declared as a virtual property
    /// </remarks>
    public class Person : DORM.Interfaces.IEntity
    {

        public long _id =0;
        public string _firstName;
        public string _surname;
        public System.DateTime _dateOfBirth;

        #region Properties
        /// <summary>
        /// The unique id of this entity
        /// </summary>
        public virtual long Id { get { return _id; } set { _id = value; } } //From interface: DORM.Interfaces.IEntity
        public virtual string Firstname { get { return _firstName; } set { _firstName = value; } }
        public virtual string Surname { get { return _surname; } set { _surname = value; } }
        public virtual System.DateTime DateOfBirth { get { return _dateOfBirth; } set { _dateOfBirth = value; } }
        #endregion

        #region Overrides
        /// <summary>
        /// Override the ToString(method) so as to show our own custom formatted string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}, {1} ({2})", Surname, Firstname, DateOfBirth.ToString("dd MMMM yyyy"));
        }
        #endregion
    }
}
