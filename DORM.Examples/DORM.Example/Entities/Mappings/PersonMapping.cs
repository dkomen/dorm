﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.Example.Entities.Mappings
{
    /// <summary>
    /// The FluentNhibernate mapping class for the Entities.Person entity
    /// </summary>
    class PersonMapping: DORM.ConcreteAccessors.FluentNhibernate.EntityMap<DORM.Example.Entities.Person>
    {
        /// <summary>
        /// 1. Create an ORM mapping between the Person entity\class and the Person table in the database
        /// 2. These mapping classes are exact FluentNHibernate mapping classes, so if you have knowledge of them then you can also do these.
        /// 3. In this class were are mapping all fields as NOT allowing to contain NULL data. The string fields are also a maximum of 20 characters in length.
        /// </summary>
        public PersonMapping()
        {
            Map(x => x.DateOfBirth).Not.Nullable();
            Map(x => x.Firstname).Not.Nullable().Length(20);
            Map(x => x.Surname).Not.Nullable().Length(20);
        }
    }
}
