﻿namespace DORM.Example
{
    partial class PersonMaintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.uiFirstname = new System.Windows.Forms.TextBox();
            this.uiSurname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uiDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // uiSave
            // 
            this.uiSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.uiSave.Location = new System.Drawing.Point(12, 101);
            this.uiSave.Name = "uiSave";
            this.uiSave.Size = new System.Drawing.Size(75, 23);
            this.uiSave.TabIndex = 0;
            this.uiSave.Text = "&Save";
            this.uiSave.UseVisualStyleBackColor = true;
            this.uiSave.Click += new System.EventHandler(this.uiSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Firstname";
            // 
            // uiFirstname
            // 
            this.uiFirstname.Location = new System.Drawing.Point(88, 12);
            this.uiFirstname.MaxLength = 20;
            this.uiFirstname.Name = "uiFirstname";
            this.uiFirstname.Size = new System.Drawing.Size(200, 20);
            this.uiFirstname.TabIndex = 2;
            // 
            // uiSurname
            // 
            this.uiSurname.Location = new System.Drawing.Point(88, 38);
            this.uiSurname.MaxLength = 20;
            this.uiSurname.Name = "uiSurname";
            this.uiSurname.Size = new System.Drawing.Size(200, 20);
            this.uiSurname.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Surname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "date of birth";
            // 
            // uiDateOfBirth
            // 
            this.uiDateOfBirth.Location = new System.Drawing.Point(88, 64);
            this.uiDateOfBirth.Name = "uiDateOfBirth";
            this.uiDateOfBirth.Size = new System.Drawing.Size(200, 20);
            this.uiDateOfBirth.TabIndex = 6;
            // 
            // PersonMaintenance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 134);
            this.Controls.Add(this.uiDateOfBirth);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.uiSurname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.uiFirstname);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.uiSave);
            this.Name = "PersonMaintenance";
            this.Text = "PersonMaintenance";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button uiSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox uiFirstname;
        private System.Windows.Forms.TextBox uiSurname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker uiDateOfBirth;
    }
}