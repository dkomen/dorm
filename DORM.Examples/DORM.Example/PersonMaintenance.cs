﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DORM.Example
{
    public partial class PersonMaintenance : Form
    {
        #region Constructors
        public PersonMaintenance()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        public Entities.Person Person { get; set; }
        #endregion

        #region Form events
        private void uiSave_Click(object sender, EventArgs e)
        {
            ///Create a new Person hide the form
            Person = new Entities.Person();
            Person.DateOfBirth = uiDateOfBirth.Value;
            Person.Firstname = uiFirstname.Text;
            Person.Surname = uiSurname.Text;
            this.Visible = false;
        }
        #endregion
    }
}
