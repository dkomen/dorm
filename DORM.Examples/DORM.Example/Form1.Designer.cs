﻿namespace DORM.Example
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiPersonsList = new System.Windows.Forms.ListBox();
            this.uiAddNewPerson = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // uiPersonsList
            // 
            this.uiPersonsList.FormattingEnabled = true;
            this.uiPersonsList.Location = new System.Drawing.Point(12, 12);
            this.uiPersonsList.Name = "uiPersonsList";
            this.uiPersonsList.Size = new System.Drawing.Size(278, 264);
            this.uiPersonsList.TabIndex = 0;
            // 
            // uiAddNewPerson
            // 
            this.uiAddNewPerson.Location = new System.Drawing.Point(12, 282);
            this.uiAddNewPerson.Name = "uiAddNewPerson";
            this.uiAddNewPerson.Size = new System.Drawing.Size(75, 23);
            this.uiAddNewPerson.TabIndex = 1;
            this.uiAddNewPerson.Text = "&Add";
            this.uiAddNewPerson.UseVisualStyleBackColor = true;
            this.uiAddNewPerson.Click += new System.EventHandler(this.uiAddNewPerson_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 310);
            this.Controls.Add(this.uiAddNewPerson);
            this.Controls.Add(this.uiPersonsList);
            this.Name = "Form1";
            this.Text = "Example usage of DORM";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox uiPersonsList;
        private System.Windows.Forms.Button uiAddNewPerson;
    }
}

