﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DORM.Example
{
    public partial class Form1 : Form
    {

        #region Fields
        private DORM.ConcreteAccessors.FluentNhibernate.Accessor<DORM.Example.Entities.Person> _dal;
        #endregion

        #region Constructors
        public Form1()
        {
            InitializeComponent();           

            ///initialise data access layer (Using the connection string in the app.config file)
            string connectionString = DORM.Example.Properties.Settings.Default.SqlConnectionString;
            _dal = new ConcreteAccessors.FluentNhibernate.Accessor<Entities.Person>(connectionString, Enumerations.DatastoreType.MsSql);

            ///On FIRST run we MUST create the database tables structures. On subsequent runs, if we made no structure\mapping class changes, the database
            ///structures will not be rebuilt or modified
            _dal.CreateSessionFactory(true, connectionString, Enumerations.DatastoreType.MsSql);
        }
        #endregion

        #region Form events
        private void Form1_Load(object sender, EventArgs e)
        {          
            UpdatePersonsList();
        }

        private void uiAddNewPerson_Click(object sender, EventArgs e)
        {
            //Show the maintenance form
            PersonMaintenance maintenanceForm = new PersonMaintenance();
            if (maintenanceForm.ShowDialog()==System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
                    {                       
                        transaction.AddOrUpdate(maintenanceForm.Person);
                        transaction.Commit();
                    }
                    UpdatePersonsList();
                }
                catch (Exception ex)
                {
                    //Oops, something went horribly wrong.
                    throw ex;
                }
            }
            
            //Close the form
            maintenanceForm.Close();
        }
        #endregion

        #region Private functions
        /// <summary>
        /// Get the latest list of Persons from the database and add them to the listbox on the form
        /// </summary>
        private void UpdatePersonsList()
        {
            try
            {
                using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
                {
                    //Retreive a List (IList = generic list interface) of type Entities.Person from the database
                    IList<Entities.Person> persons = transaction.Retrieve<Entities.Person>();

                    //Set the data of the listbox named uiPersonsList to the database retrieval result
                    uiPersonsList.DataSource = persons;
                }
            }
            catch (Exception ex)
            {
                //Oops, something went horribly wrong.
                MessageBox.Show(ex.ToString(), "An error occured");
                throw ex;
            }
        }
        #endregion        
    }
}
