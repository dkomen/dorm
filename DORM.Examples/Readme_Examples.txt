Solution tested using the Professional versions of Microsoft Visual Studio 2012 and 2013

All examples use the FluentNhibernate accessor.

1.The first simpler example project is: DORM.Example
2.The second more complex example project is: DORM.ExampleAdvanced which itself references DORM.ExampleAdvanced.Entities

The examples assume that you have a Microsoft SQL Server database already created and named: DataStorage. They also assume that you have windows logon credential rights to this database.
If you have a differently named database or logon security settings then please change the connection strings in the app.config files accordingly.

Enjoy.