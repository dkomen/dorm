﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DORM.ExampleAdvanced
{
    public partial class Form1 : Form
    {
        #region Fields
        private DORM.ConcreteAccessors.FluentNhibernate.Accessor<DORM.ExampleAdvanced.Entities.PersonAdvanced> _dal;
        private string _connectionString = DORM.ExampleAdvanced.Properties.Settings.Default.SqlConnectionString;
        #endregion

        #region Constructors
        public Form1()
        {
            InitializeComponent();

            ///initialise data access layer            
            _dal = new ConcreteAccessors.FluentNhibernate.Accessor<Entities.PersonAdvanced>(_connectionString, Enumerations.DatastoreType.MsSql);
        }
        #endregion

        #region Form events
        /// <summary>
        /// Start the example here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                #region On first run we create the database tables if needed. True if to create, else false to not create
                CreateTheDatabaseTables(true, _connectionString, Enumerations.DatastoreType.MsSql);
                #endregion
                
                #region Have we already created the database tables and their data?
                bool alreadyCreated = false;
                using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
                {
                    alreadyCreated = (transaction.RetrieveFirst<Entities.PersonAdvanced_Fast>(null) != null);
                }
                if (alreadyCreated == false)
                {

                    #region Create 6 person entities
                    Entities.PersonAdvanced personOne = new Entities.PersonAdvanced() { DateOfBirth = new DateTime(1970, 01, 25), Firstname = "Dean", Surname = "Smith", HairColour = Entities.Enumerations.HairColour.Brown };
                    Entities.PersonAdvanced personTwo = new Entities.PersonAdvanced() { DateOfBirth = new DateTime(1974, 03, 4), Firstname = "Jeff", Surname = "McDonald", HairColour = Entities.Enumerations.HairColour.Brown };
                    Entities.PersonAdvanced personThree = new Entities.PersonAdvanced() { DateOfBirth = new DateTime(1992, 06, 19), Firstname = "Sally", Surname = "Lucas", HairColour = Entities.Enumerations.HairColour.Blond, BestFriend = personTwo };
                    Entities.PersonAdvanced personFour = new Entities.PersonAdvanced() { DateOfBirth = new DateTime(1998, 2, 3), Firstname = "Marcia", Surname = "Brown", HairColour = Entities.Enumerations.HairColour.Red, BestFriend = personOne };
                    Entities.PersonAdvanced personFive = new Entities.PersonAdvanced() { DateOfBirth = new DateTime(1982, 11, 6), Firstname = "Steven", Surname = "Holts", HairColour = Entities.Enumerations.HairColour.Black };
                    Entities.PersonAdvanced personSix = new Entities.PersonAdvanced() { DateOfBirth = new DateTime(1982, 11, 6), Firstname = "Stephanie", Surname = "Smith", HairColour = Entities.Enumerations.HairColour.Blond, BestFriend = personOne };
                    #endregion

                    #region Add new records: Add all six persons to the database in a single transaction
                    using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
                    {
                        transaction.AddOrUpdate(personOne);
                        transaction.AddOrUpdate(personTwo);
                        transaction.AddOrUpdate(personThree);
                        transaction.AddOrUpdate(personFour);
                        transaction.AddOrUpdate(personFive);
                        transaction.AddOrUpdate(personSix);

                        transaction.Commit();
                    }
                    #endregion

                }
                #endregion               
                
                #region Using Filters and updating a record: Get person with Firstname being 'Dean' from the database and add a new best friend to him: Stephanie
                using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
                {
                    #region Create filter
                    DORM.Filter filter = new Filter();
                    string fieldName = DORM.HelperMethods.GetPropertyName(() => new Entities.PersonAdvanced().Firstname); //Get a type safe field name.. versus just: string fieldName = "Surname";
                    //string fieldName = "Firstname"; //Non-type safe way

                    filter.Create(fieldName, Enumerations.FilterCriteriaComparitor.Equals, "Dean");
                    //Or as an alternate way you could do it like so:
                    //FilterItem fItem = new FilterItem(null);
                    //filter.Create(fItem.Add(fieldName, Enumerations.FilterCriteriaComparitor.Equals, "Dean"));

                    #endregion
                    Entities.PersonAdvanced dean = transaction.RetrieveFirst<Entities.PersonAdvanced>(filter);

                    filter.Create(fieldName, Enumerations.FilterCriteriaComparitor.Equals, "Stephanie");
                    Entities.PersonAdvanced stephanie = transaction.RetrieveFirst<Entities.PersonAdvanced>(filter);

                    dean.BestFriend = stephanie;

                    transaction.AddOrUpdate(dean);

                    transaction.Commit();
                }
                #endregion

                #region Rollback: Retreive then delete all Persons born after 1994 that have no best friends assigned, there should be one of them: Marcia
                using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
                {
                    #region Create filter
                    DORM.Filter filter = new Filter();
                    FilterItem fItem = new FilterItem(null);
                    #region Create a filter similar to the SQL statement: SELECT * FROM PersonAdvanced WHERE (BestFriendId IS NOT NULL) AND (DateOfBirth > '1 January 1995')
                    filter.Create(
                            fItem.Add(DORM.HelperMethods.GetPropertyName(() => new Entities.PersonAdvanced().BestFriend), Enumerations.FilterCriteriaComparitor.NotEquals, null)
                            .SetJoinType(Enumerations.FilterJoinTypeRegion.And)
                            .Add(DORM.HelperMethods.GetPropertyName(() => new Entities.PersonAdvanced().DateOfBirth), Enumerations.FilterCriteriaComparitor.GreaterThan, new DateTime(1995, 1, 1))
                            );
                    #endregion
                    #endregion

                    IList<Entities.PersonAdvanced> friendlessPersons = transaction.Retrieve<Entities.PersonAdvanced>(filter);

                    foreach (Entities.PersonAdvanced person in friendlessPersons)
                    {
                        transaction.Delete(person);
                    }

                    //We changed our minds.. lets not delete them anymore.. do a rollback
                    //transaction.Commit();
                    transaction.Rollback();
                }
                #endregion

                MessageBox.Show("Successfully completed the demo!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Oops, an error happended! Error:" + ex.ToString());
                throw;
            }
        }
        #endregion

        #region Private functions
        /// <summary>
        /// Create the database tables based on the mapping files. The database as specified in the connection string should already exist
        /// </summary>
        /// <remarks>Remember that this examples assume that you have a access to a Microsoft SQL Server database already created and named: DataStorage</remarks>
        /// <param name="createOrUpdateTheDatabaseTables"></param>
        /// <param name="connectionString"></param>
        /// <param name="datastoreType">The type of back-end database to connect to</param>
        private void CreateTheDatabaseTables(bool createOrUpdateTheDatabaseTables, string connectionString, Enumerations.DatastoreType datastoreType)
        {
            if(createOrUpdateTheDatabaseTables)
            {
                _dal.CreateSessionFactory(true, connectionString, datastoreType);
            }
        }
        #endregion
    }
}
