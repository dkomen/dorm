﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.ExampleAdvanced.Entities.Enumerations
{
    /// <summary>
    /// Possible hair colours to choose from
    /// </summary>
    public enum HairColour
    {
        Brown = 1,
        Blond = 2,
        Black = 3,
        Red = 4
    }
}
