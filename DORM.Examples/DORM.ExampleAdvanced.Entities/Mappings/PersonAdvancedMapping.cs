﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.ExampleAdvanced.Entities.Mappings
{
    /// <summary>
    /// The FluentNhibernate mapping class for the Entities.Person entity
    /// </summary>
    /// <remarks>
    /// Nice convention to follow: Use 'this.GetType().Name' in a mapping class as the index or key name when manually needing to create indexes and unique keys, example: string index = "IDX" + this.GetType().Name + "_FieldName"
    /// 
    /// Tutorial
    /// --------
    /// 1.ONE-TO-MANY RELATIONSHIPS
    /// 	On master mapping class use 'HasMany'
    /// 	On child mapping class use 'References'
    /// 	
    /// 	Example: A School object containing multiple Student objects (in a normal IList<Student> property)
    /// 	 On School mapping : Add statement 'HasMany<Student>(x => x.Students);'
    /// 	 On Student Mapping : Add statement 'References<School>(x => x.School);'
    /// 
    /// 2.DECLARING A MULTI_COLUMN UNIQUE KEY 
    /// 	In the mapping class add '.UniqueKey(this.GetType().Name)' to each of the mapping statements to include in the key
    /// 
    /// 3.DECLARING A MULTI_COLUMN INDEX
    /// 	In the mapping class add '.Index(this.GetType().Name)' to each of the mapping statements to include in the index
    /// 
    /// 4.DECLARING A FIELD AS AN ENUMERATION
    ///   Declaring a field with an enum type (eg. enumeration of eye colours named EyeColor) but you want to save the selected items
    ///   integer value and NOT the default string value (eg Blue) would require '.CustomType<EyeColor>()' at the end of the mapping statement
    /// </remarks>
    class PersonAdvancedMapping : DORM.ConcreteAccessors.FluentNhibernate.EntityMap<DORM.ExampleAdvanced.Entities.PersonAdvanced>
    {
        /// <summary>
        /// 1. Create an ORM mapping between the Person entity\class and the Person table in the database
        /// 2. These mapping classes are exact FluentNHibernate mapping classes, so if you have knowledge of them then you can also do these.
        /// </summary>
        public PersonAdvancedMapping()
        {
            Map(x => x.DateOfBirth).Not.Nullable();
            Map(x => x.Firstname).Not.Nullable().Length(20);
            Map(x => x.Surname).Not.Nullable().Length(20).Index("IDX_" + this.GetType().Name + "_Surname");
            Map(x => x.HairColour).Nullable().CustomType<Enumerations.HairColour>(); 
            References<Entities.PersonAdvanced>(x => x.BestFriend).Nullable();
        }
    }
}
