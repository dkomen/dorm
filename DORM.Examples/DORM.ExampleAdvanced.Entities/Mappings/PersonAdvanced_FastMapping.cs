﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.ExampleAdvanced.Entities.Mappings
{    
    /// <summary>
    /// Mapping class for quick access to PersonAdvanced table    
    /// </summary>
    /// <remarks>
    /// We dont want this mapping class to cause the generation SQL schema information (we don't want to create the table in SQL) as the mapping class
    /// named PersonAdvancedMapping should be used to create the table with. The reason is that the PersonAdvancedMapping class specifies ALL of the fields
    /// to map whereas this PersonAdvanced_FastMapping contains only a subset if fields.. namely only the Id field.
    /// 
    /// In order for this mapping classes table to not be generated we include the code: SchemaAction.None();
    /// 
    /// </remarks>
    class PersonAdvanced_FastMapping : DORM.ConcreteAccessors.FluentNhibernate.EntityMap<DORM.ExampleAdvanced.Entities.PersonAdvanced_Fast>
    {
        public PersonAdvanced_FastMapping()
        {
            Table("PersonAdvanced");//Use the table named 'PersonAdvanced' instead of 'PersonAdvanced_Fast'
            SchemaAction.None(); //Do not generate the table
        }
    }
}
