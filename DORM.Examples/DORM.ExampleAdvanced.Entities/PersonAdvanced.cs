﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.ExampleAdvanced.Entities
{
    /// <summary>
    /// A entity that contains a person's details
    /// </summary>
    /// <remarks>
    /// Any property that will actually map to the database must be declared as a virtual property
    /// </remarks>
    public class PersonAdvanced: DORM.Interfaces.IEntity
    {
        #region Properties
        /// <summary>
        /// The unique id of this entity
        /// </summary>
        public virtual long Id { get; set; } //From interface: DORM.Interfaces.IEntity
        public virtual string Firstname { get; set; }
        public virtual string Surname { get; set; }
        public virtual System.DateTime DateOfBirth { get; set; }
        public virtual Enumerations.HairColour HairColour { get; set; }

        /// <summary>
        /// The person's best friend
        /// </summary>
        public virtual PersonAdvanced BestFriend { get; set; }
        #endregion

        #region Overrides
        /// <summary>
        /// Override the ToString(method) so as to show our own custom formatted string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}, {1} ({2})", Surname, Firstname, DateOfBirth.ToString("dd MMMM yyyy"));
        }
        #endregion
    }
}
