﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DORM.ExampleAdvanced.Entities
{
    /// <summary>
    /// A very small entity that can be used to retrieve (or update) just the very basics of a persons
    /// </summary>    
    public class PersonAdvanced_Fast: DORM.Interfaces.IEntity
    {
        #region Properties
        /// <summary>
        /// The unique id of this entity
        /// </summary>
        public virtual long Id { get; set; } //From interface: DORM.Interfaces.IEntity
        #endregion
    }
}
